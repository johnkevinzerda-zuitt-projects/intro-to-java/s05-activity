package com.zuitt;

import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts = new ArrayList<> ();

    public Phonebook() {}

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void displayContacts() {
        if (contacts.size() == 0) {
            System.out.println("Phonebook is Empty.");
        } else {
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(String.format("%s has the following registered numbers:", contact.getName()));
                for (String number : contact.getNumbers()) {
                    System.out.println(number);
                }
                System.out.println("--------------------");
                System.out.println(String.format("%s has the following registered addresses:", contact.getName()));
                for (String address : contact.getAddresses()) {
                    System.out.println(address);
                }
                System.out.println("====================");
            }
        }
    }
}

