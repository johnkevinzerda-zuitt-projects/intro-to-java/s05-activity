package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;


public class Main {

    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook ();

        ArrayList<String> numbers1 = new ArrayList<>(Arrays.asList("123", "123", "123"));
        ArrayList<String> address1 = new ArrayList<>(Arrays.asList("abc", "abc", "abc"));
        ArrayList<String> numbers2 = new ArrayList<>(Arrays.asList("456", "456", "456"));
        ArrayList<String> address2 = new ArrayList<>(Arrays.asList("def", "def", "def"));

        Contact contact1 = new Contact("john", numbers1, address1);
        Contact contact2 = new Contact("Aria", numbers2, address2);

        ArrayList<Contact> contacts = new ArrayList<>(Arrays.asList(contact1, contact2));

        phonebook.setContacts(contacts);
        phonebook.displayContacts();

    }
}
